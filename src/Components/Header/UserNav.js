import React from "react";
import { NavLink } from "react-router-dom";
import {
  GithubOutlined,
  LoginOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
export default function UserNav() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const handleLogout = () => {
    userLocalService.remove();
    window.location.reload();
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <div className="flex justify-center items-center font-bold gap-1 border-r-2 px-2 text-orange-500">
            <GithubOutlined />
            <span>
              {user?.hoTen.length > 5 ? user?.hoTen.slice(0, 5) : user?.hoTen}
            </span>
          </div>

          <button
            onClick={handleLogout}
            className="flex justify-center items-center font-bold gap-1 px-2 hover:text-orange-500 transition duration-300"
          >
            <LogoutOutlined />
            <span className="hidden md:inline-flex">Đăng Xuất</span>
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink
            to="/login"
            className=" flex justify-center items-center font-bold gap-1 border-r-2 px-2 hover:text-orange-500 transition duration-300"
          >
            <LoginOutlined />
            <span className="hidden md:inline-flex"> Đăng Nhập</span>
          </NavLink>
          <NavLink
            to="/register"
            className="flex justify-center items-center font-bold gap-1 px-2 hover:text-orange-500 transition duration-300"
          >
            <GithubOutlined />
            <span className="hidden md:inline-flex"> Đăng Ký</span>
          </NavLink>
        </>
      );
    }
  };
  return (
    <div className="flex justify-center items-center gap-1">
      {renderContent()}
    </div>
  );
}
