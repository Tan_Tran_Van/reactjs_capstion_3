import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import Lottie from "lottie-react";
import loginMovie from "../../assets/login-movie.json";

export default function Header() {
  return (
    <div className="py-5 px-6 flex justify-between items-center shadow-lg bg-white">
      <NavLink to="/" className="flex items-center">
        <Lottie className="h-14 mr-3" animationData={loginMovie} />
        <span className="font-bold text-orange-400 text-2xl sm:text-4xl lg:text-5xl hover:text-orange-500">
          TVTMovie
        </span>
      </NavLink>
      <nav className="flex sm:justify-center space-x-1">
        {[
          ["Danh Sách Phim", "#movieList"],
          ["Lịch Chiếu", "#theater"],
          ["Rạp Phim", "#theater"],
          ["Thông Tin", "#footer"],
        ].map(([title, url]) => (
          <a
            href={url}
            className="hidden md:inline-flex rounded-lg px-1 lg:px-3 py-2 font-medium lg:font-bold hover:bg-slate-100 hover:text-orange-500 transition duration-300"
          >
            {title}
          </a>
        ))}
      </nav>
      <UserNav />
    </div>
  );
}
