import React from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function Spinner() {
  let isLoadding = useSelector((state) => {
    return state.spinnerSlice.isLoading;
  });
  return isLoadding ? (
    <div className="fixed bg-orange-100 t-0 l-0 h-screen w-screen z-50 flex justify-center items-center">
      <RingLoader color="orange" size="150" speedMultiplier={2} />
    </div>
  ) : (
    <></>
  );
}
