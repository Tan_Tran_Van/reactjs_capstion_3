import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { movieService } from "../../service/movieService";
import { NavLink } from "react-router-dom";
import { message } from "antd";

export default function DetailPage() {
  let navigate = useNavigate();
  let params = useParams();
  // console.log("params: ", params);
  const [detailMovie, setDetailMovie] = useState([]);
  useEffect(() => {
    movieService
      .getDetailMovie(params.id)
      .then((res) => {
        // console.log("res detail moive: ", res);
        setDetailMovie(res.data.content);
      })
      .catch((err) => {
        message.error("Không tìm thấy dữ liệu");
        // console.log("err: ", err);
        setTimeout(() => {
          navigate("/");
        }, 1000);
      });
  }, []);

  const renderDetailMoive = () => {
    return (
      <div className="flex justify-center items-center gap-2 md:gap-10 p-1 md:p-10 h-screen">
        <img
          className="h-96 w-1/3 md:w-1/2 object-center"
          alt={detailMovie?.biDanh}
          src={detailMovie?.hinhAnh}
        />
        <div className="flex flex-col justify-center h-full">
          <h1 className="text-orange-500 text-xl md:text-2xl lg:text-3xl font-bold">
            {detailMovie?.tenPhim?.toUpperCase()}
          </h1>
          <p className="text-white py-2">{detailMovie?.moTa}</p>
          <button className="w-1/2 transition duration-300 font-bold text-xl md:text-2xl bg-orange-300 text-white px-3 md:px-10 py-2 rounded hover:bg-orange-400 hover:text-white ">
            Đặt Vé
          </button>
        </div>
      </div>
    );
  };
  return (
    <div className="flex justify-center items-center h-screen text-white bg-img-moive bg-cover bg-no-repeat bg-center">
      {renderDetailMoive()}
    </div>
  );
}
