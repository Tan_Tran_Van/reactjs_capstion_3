import React from "react";

export default function NotFoundPage() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <h1 className="text-center text-red-800 text-5xl font-black animate-bounce">
        404 Page
      </h1>
    </div>
  );
}
