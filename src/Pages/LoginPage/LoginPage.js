import React from "react";
import { Form, Input, message } from "antd";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import Lottie from "lottie-react";
import loginMovie from "../../assets/login-movie.json";
import { NavLink, useNavigate } from "react-router-dom";
import { userService } from "../../service/userService";
import { userLocalService } from "../../service/localService";
import { useDispatch } from "react-redux";
import { setUserInfo } from "../../redux-toolkit/userSlice";
export default function LoginPage() {
  let naigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    // console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        // console.log("res: ", res);
        message.success("Đăng Nhập Thành Công");
        dispatch(setUserInfo(res.data.content));
        userLocalService.set(res.data.content);

        setTimeout(() => {
          naigate("/");
        }, 1000);
      })
      .catch((err) => {
        // console.log("err: ", err);
        message.error("Đăng Nhập Thất Bại. Vui lòng đăng nhập lại!");
        setTimeout(() => {
          window.location.reload();
          // naigate("/login");
        }, 1000);
      });
  };
  const onFinishFailed = (errorInfo) => {
    // console.log('errorInfo: ', errorInfo);
    message.error("Đăng Nhập Thất Bại. Vui lòng đăng nhập lại!");
  };
  return (
    <>
      <NavLink to="/" className="fixed top-0 left-0 p-2 flex items-center">
        <Lottie className="h-14 mr-3" animationData={loginMovie} />
        <span className=" font-bold text-orange-400 text-5xl hover:text-orange-500">
          TVTMovie
        </span>
      </NavLink>
      <div className="pt-10 px-3 flex justify-center items-center bg-img-moive bg-cover bg-no-repeat bg-center h-screen">
        <div className="w-screen sm:w-1/2 bg-black bg-opacity-75 rounded p-5">
          <Lottie className="w-20 md:w-40 m-auto" animationData={loginMovie} />
          <h1 className="text-orange-400 text-3xl md:text-5xl text-center">
            Đăng Nhập
          </h1>
          <Form
            className="text-white text-xl"
            layout="vertical"
            name="basic"
            labelCol={{
              span: 12,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <p className="text-white text-xl p-2">Tài Khoản</p>
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Tài Khoản của bạn!",
                },
              ]}
            >
              <Input
                className="text-2xl"
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Tài Khoản"
              />
            </Form.Item>

            <p className="text-white text-xl p-2">Mật Khẩu</p>

            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Mật Khẩu của bạn!",
                },
              ]}
            >
              <Input.Password
                className=" text-2xl"
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Mật Khẩu"
              />
            </Form.Item>

            <Form.Item
              className="text-center"
              wrapperCol={{
                span: 24,
              }}
            >
              <button className="m-auto bg-orange-400 text-white text-2xl text-center rounded shadow px-10 py-3 hover:bg-orange-600">
                ĐĂNG NHẬP
              </button>
            </Form.Item>
            <div className="text-right">
              <span>Bạn chưa có tài khoản? </span>
              <NavLink
                to="/register"
                className="text-orange-300 hover:text-orange-500"
              >
                {" "}
                Đăng ký ngay
              </NavLink>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
}
