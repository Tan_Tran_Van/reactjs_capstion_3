import React, { useEffect, useState } from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
import { movieService } from "../../../service/movieService";
const { Meta } = Card;

export default function Movielist() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {});
  }, []);

  const renderMovieList = () => {
    return movieArr.slice(0, 12).map((item) => {
      return (
        <Card
          hoverable
          className="overflow-hidden"
          cover={
            <img
              className="h-96 object-top transition duration-150 ease-in-out overflow-y-hidden hover:scale-110"
              alt={item.biDanh}
              src={item.hinhAnh}
            />
          }
        >
          <Meta
            className="py-5"
            title={
              <h1 className="text-orange-500">{item.tenPhim.toUpperCase()}</h1>
            }
            description={
              item.moTa < 100 ? item.moTa : item.moTa.slice(0, 100) + "..."
            }
          />
          <NavLink
            to={`/detail/${item.maPhim}`}
            className="w-full transition duration-300 bg-orange-300 text-white px-10 py-2 rounded hover:bg-orange-400 hover:text-white "
          >
            Xem Chi Tiết
          </NavLink>
        </Card>
      );
    });
  };

  return (
    <div
      id="movieList"
      className="container py-2 md:py-5 grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 m-auto gap-5 "
    >
      {renderMovieList()}
    </div>
  );
}
