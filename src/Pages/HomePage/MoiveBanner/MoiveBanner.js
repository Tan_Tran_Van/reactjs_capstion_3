import React from "react";
import { Carousel } from "antd";
// const contentStyle = {
//   height: "160px",
//   color: "#fff",
//   lineHeight: "160px",
//   textAlign: "center",
//   background: "#364d79",
// };

export default function MoiveBanner({ movieBannerArr }) {
  const renderMovieBanner = () => {
    return movieBannerArr.map((item) => {
      return (
        <div className="w-screen">
          <img
            className="object-center object-cover h-screen w-screen"
            src={item.hinhAnh}
            alt={item.maPhim}
          />
        </div>
      );
    });
  };
  return <Carousel autoplay>{renderMovieBanner()}</Carousel>;
}
