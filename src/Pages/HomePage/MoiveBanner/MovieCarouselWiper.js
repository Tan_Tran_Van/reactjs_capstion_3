import React, { useEffect, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";
import { movieService } from "../../../service/movieService";

export default function MovieCarouselWiper() {
  const [movieBannerArr, setMovieBannerArr] = useState([]);
  useEffect(() => {
    movieService
      .getMovieBanner()
      .then((res) => {
        setMovieBannerArr(res.data.content);
      })
      .catch((err) => {});
  }, []);
  const renderMovieCarouselWiper = () => {
    return movieBannerArr.map((item) => {
      return (
        <SwiperSlide>
          <img
            className="h-full w-full object-top object-fill "
            src={item.hinhAnh}
            alt={item.maPhim}
          />
        </SwiperSlide>
      );
    });
  };
  return (
    <Swiper
      className="h-1/2 sm:h-screen"
      spaceBetween={30}
      centeredSlides={true}
      loop={true}
      autoplay={{
        delay: 3500,
        disableOnInteraction: false,
      }}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Autoplay, Pagination, Navigation]}
      style={{
        // height: 800,
        "--swiper-pagination-color": "#fff",
        "--swiper-navigation-color": "#fff",
      }}
    >
      {renderMovieCarouselWiper()}
    </Swiper>
  );
}
