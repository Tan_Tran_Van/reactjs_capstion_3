import React, { useEffect, useState } from "react";
import { movieService } from "../../../service/movieService";
import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";

const onChange = (key) => {};
export default function MovieTab() {
  const [dataMovieByTheater, setDataMovieByTheater] = useState([]);
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        // console.log("res theater: ", res);
        setDataMovieByTheater(res.data.content);
      })
      .catch((err) => {
        // console.log("err: ", err);
      });
  }, []);

  const renderListMoive = (listMovie) => {
    return listMovie.map((movie) => {
      return <MovieItemTab movie={movie} />;
    });
  };

  const renderListTheater = (listTheater) => {
    // console.log("listTheater: ", listTheater);
    return listTheater.map((theater) => {
      return {
        key: theater.maCumRap,
        label: (
          <h2 className="w-20 md:40 lg:w-64 sm:font-medium lg:font-bold sm:mx-2 sm:py-2 border-b-2">
            {theater.tenCumRap.split(" ")[0]}{" "}
            {
              theater.tenCumRap.split(" - ")[
                theater.tenCumRap.split(" - ").length - 1
              ]
            }
          </h2>
        ),
        children: (
          <div
            style={{ height: 600, overflowY: "scroll" }}
            // className="h-full overflow-y-scroll"
          >
            {renderListMoive(theater.danhSachPhim)}
          </div>
        ),
      };
    });
  };

  const renderTheater = () => {
    return dataMovieByTheater.map((listTheater) => {
      return {
        key: listTheater.maHeThongRap,
        label: (
          <img
            className="h-14 w-14 sm:h-16 sm:w-16 m-0 sm:mx-10"
            src={listTheater.logo}
            alt=""
          />
        ),
        children: (
          <Tabs
            style={{ height: 600 }}
            // className="h-full"
            tabPosition="left"
            defaultActiveKey="1"
            items={renderListTheater(listTheater.lstCumRap)}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div id="theater" className="container py-5 md:py-10 m-auto">
      <h1 className="text-center font-bold text-orange-500 text-4xl p-4">
        LỊCH CHIẾU PHIM
      </h1>
      <Tabs
        className="py-10 border-2 rounded-xl shadow-xl"
        // tabPosition="left"
        style={{ height: 800, overflow: "hidden" }}
        defaultActiveKey="1"
        items={renderTheater()}
        onChange={onChange}
      />
    </div>
  );
}
