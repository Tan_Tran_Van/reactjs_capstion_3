import React from "react";
import moment from "moment";
import "moment/locale/vi";
moment.locale("vi");
export default function MovieItemTab({ movie }) {
  return (
    <div className="flex mt-10 space-x-5">
      <img
        className="hidden sm:inline-flex sm:h-40 sm:w-20 md:h-60 md:w-44 object-cover"
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        <h3 className="font-normal md:font-bold text-orange-600 text-xl py-3">
          {movie.tenPhim}
        </h3>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu) => {
            return (
              <button className="py-2 px-2 border rounded font-normal md:font-medium transition duration-300 hover:bg-orange-500 hover:text-white">
                {moment(lichChieu.ngayChieuGioChieu).format(
                  "DD-MM-YYYY ~ hh:mm "
                )}
              </button>
            );
          })}
        </div>
      </div>
    </div>
  );
}
