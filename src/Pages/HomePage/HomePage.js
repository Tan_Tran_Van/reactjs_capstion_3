import React from "react";
import MovieCarouselWiper from "./MoiveBanner/MovieCarouselWiper";
import Movielist from "./Movielist/Movielist";
import MovieTab from "./MoiveTab/MovieTab";

export default function HomePage() {
  return (
    <div>
      <MovieCarouselWiper />
      <Movielist />
      <MovieTab />
    </div>
  );
}
