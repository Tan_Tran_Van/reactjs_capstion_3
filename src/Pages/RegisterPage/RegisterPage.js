import React from "react";
import { Form, Input, message } from "antd";
import {
  LockOutlined,
  UserOutlined,
  MailOutlined,
  PhoneOutlined,
  FontSizeOutlined,
} from "@ant-design/icons";
import Lottie from "lottie-react";
import loginMovie from "../../assets/login-movie.json";
import { NavLink, useNavigate } from "react-router-dom";
import { userService } from "../../service/userService";

export default function RegisterPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    // values = { ...values, maLoaiNguoiDung: "KhanhHang" };
    // console.log("Success:", values);
    userService
      .postRegister(values)
      .then((res) => {
        console.log("res: ", res);
        message.success("Đăng ký thành công");
        setTimeout(() => {
          navigate("/login");
        }, 2000);
      })
      .catch((err) => {
        console.log("err: ", err);

        message.error(`Đăng ký thất bại, ${err.response.data.content}`);

        setTimeout(() => {
          window.location.reload();
          // navigate("/register");
        }, 1000);
      });
  };
  const onFinishFailed = (errorInfo) => {
    message.error("Đăng ký thất bại, Vui lòng kiểm tra lại thông tin");
  };
  return (
    <div>
      <NavLink to="/" className="fixed top-0 left-0 p-2 flex items-center">
        <Lottie className="h-14 mr-3" animationData={loginMovie} />
        <span className="font-bold text-orange-400 text-2xl sm:text-4xl lg:text-5xl hover:text-orange-500">
          TVTMovie
        </span>
      </NavLink>

      <div className="flex justify-center items-center bg-img-moive bg-cover bg-no-repeat bg-center p-10">
        <div className="w-screen sm:w-1/2 bg-black bg-opacity-75 rounded p-5">
          <h1 className="text-orange-400 text-5xl text-center">Đăng Ký</h1>
          <Form
            className="text-white "
            layout="vertical"
            name="basic"
            labelCol={{
              span: 12,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <p className="text-white px-2 py-1">Tài Khoản</p>
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Tên Tài Khoản bạn muốn!",
                },
              ]}
            >
              <Input
                className="text-xl"
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Tài Khoản"
              />
            </Form.Item>
            <p className="text-white px-2 py-1">Mật Khẩu</p>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Mật Khẩu của bạn!",
                },
              ]}
            >
              <Input.Password
                className=" text-xl"
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Mật Khẩu"
              />
            </Form.Item>
            <p className="text-white px-2 py-1">Email</p>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Email của bạn!",
                },
              ]}
            >
              <Input
                className="text-xl"
                prefix={<MailOutlined className="site-form-item-icon" />}
                type="email"
                placeholder="Email"
              />
            </Form.Item>
            <p className="text-white px-2 py-1">Họ Tên</p>
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Họ Tên của bạn!",
                },
              ]}
            >
              <Input
                className="text-xl"
                prefix={<FontSizeOutlined className="site-form-item-icon" />}
                type="text"
                placeholder="Họ Tên"
              />
            </Form.Item>
            <p className="text-white px-2 py-1">Số Điện Thoại</p>
            <Form.Item
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Số Điện Thoại của bạn!",
                },
              ]}
            >
              <Input
                className=" text-xl"
                prefix={<PhoneOutlined className="site-form-item-icon" />}
                type="text"
                placeholder="Số Điện Thoại"
              />
            </Form.Item>

            <Form.Item className="text-center">
              <button className="m-auto bg-orange-400 text-white text-xl text-center rounded shadow px-10 py-3 hover:bg-orange-600">
                ĐĂNG KÝ
              </button>
            </Form.Item>

            <Form.Item className="text-right">
              <span className="text-white text-xl">Bạn đã có tài khoản? </span>
              <NavLink
                to="/login"
                className="text-orange-300 text-xl hover:text-orange-500"
              >
                {" "}
                Quay Lại Đăng nhập
              </NavLink>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
