import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
};

const spinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    isLoadingOn: (state, action) => {
      state.isLoading = true;
    },
    isLoadingOff: (state, action) => {
      state.isLoading = false;
    },
  },
});
export const { isLoadingOn, isLoadingOff } = spinnerSlice.actions;
export default spinnerSlice.reducer;
