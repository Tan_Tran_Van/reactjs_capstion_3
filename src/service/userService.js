import { https } from "./configURL";

export const userService = {
  postLogin: (data) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", data);
  },
  postRegister: (data) => {
    return https.post("/api/QuanLyNguoiDung/DangKy", data);
  },
  // getRegister: () => {
  //   return https.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung");
  // },
};
