/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        "img-moive": "url('/public/img/backgroundMoive.jpg')",
      },
    },
  },
  plugins: [],
};
